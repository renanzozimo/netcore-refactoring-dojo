
# GildedRose JS

Resultado do teste proposto em .net porém em JS


## Estrutura

Os resultados de refactoring estão na pasta `./src/entidades`.

Criada uma pasta `_dist` que contém o resultado do build com um index.html. Esse index pode ser aberto diretamente no browser como um arquivo html local.

O objetivo é oferecer um render simples e rápido da solução para que se possa facilmente comparar o resultado com `ApprovalTest.TrintaDias.approved.txt`.


## Comandos

* `npm run build`: Gera o arquivo ./_dist/index.js a partir de src/index.ts
* `npm run test`: Testes utilizando jasmine
* `npm run lint`: (Desenvolvimento) revisão de lint utilizando ESLint
* `npm run lint-fix`: (Desenvolvimento) revisão de lint utilizando ESLint + correções com as quais ele consegue lidar sozinho


## Nota Final

Uma palavra, Rogerinho: Show!