import { GildedRose } from './entidades/GildedRose';
import { Item } from './entidades/Item';

const contentElement = document.getElementById('content');
const textareaElement: HTMLTextAreaElement =
  document.getElementById('textarea') as HTMLTextAreaElement;
let dayCount = 0;

const render = (novaLista: GildedRose) => {
  if (contentElement) {
    const resultText = getResultText(novaLista.itens);
    contentElement.innerHTML = contentElement.innerHTML + resultText;
    textareaElement.value = textareaElement.value + resultText;
    ++dayCount;
  }
};

const getResultText = (items: Item[]): string => {
  let result: string =
`-------- dia ${dayCount} --------
Nome, PrazoParaVenda, Qualidade`;
  for (const item of items) {
    result += `
${item.nome}, ${item.prazoParaVenda}, ${item.qualidade}`;
  }
  result += `

`;
  return result;
};

const update = (gildedRose: GildedRose) => {
  gildedRose.atualizarQualidade();
  render(gildedRose);
  window.scrollTo(0,document.body.scrollHeight);
};

const update30 = (gildedRose: GildedRose) => {
  let n: number = 0;
  while (n < 30) {
    n++;
    gildedRose.atualizarQualidade();
    render(gildedRose);
  }
  window.scrollTo(0,document.body.scrollHeight);
};

const copyText = () => {
  textareaElement.select();
  textareaElement.setSelectionRange(0, 9999999);
  document.execCommand('copy');
  alert('Resultado copiado para área de transferência');
};

const list = new GildedRose([
  new Item('Corselete +5 DEX', 10, 20),
  new Item('Queijo Brie Envelhecido', 2, 0),
  new Item('Elixir do Mangusto', 5, 7),
  new Item('Dente do Tarrasque', 0, 80),
  new Item('Dente do Tarrasque', -1, 80),
  new Item('Ingressos para o concerto do Turisas', 15, 20),
  new Item('Ingressos para o concerto do Turisas', 10, 49),
  new Item('Ingressos para o concerto do Turisas', 5, 49),
  new Item('Bolo de Mana Conjurado', 3, 6),
]);

render(list);

document.getElementById('btn')?.addEventListener('click', () => update(list));
document.getElementById('btn30')?.addEventListener('click', () => update30(list));
document.getElementById('btnCopy')?.addEventListener('click', () => copyText());
