export class MathHelpers {
  static limitarValor(min: number, valor: number, max: number): number {
    return Math.max(min, Math.min(valor, max));
  }
}
