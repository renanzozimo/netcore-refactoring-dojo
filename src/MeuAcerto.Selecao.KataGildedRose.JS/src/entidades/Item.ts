export class Item {
  nome: string;
  prazoParaVenda: number;
  qualidade: number;

  constructor(
    nome: string,
    prazoParaVenda: number,
    qualidade: number
  ) {
    this.nome = nome;
    this.prazoParaVenda = prazoParaVenda;
    this.qualidade = qualidade;
  }
}
