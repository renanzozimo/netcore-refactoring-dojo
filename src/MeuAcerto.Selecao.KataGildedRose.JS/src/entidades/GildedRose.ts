import { IngressoParaConcertoTurisas } from './IngressoParaConcertoTurisas';
import { Item } from './Item';
import { ItemAtualizavel } from './ItemAtualizavel';
import { QueijoBrieEnvelhecido } from './QueijoBrieEnvelhecido';

export class GildedRose {
  itens: Item[];

  constructor(list: Item[]) {
    this.itens = list;
  }

  atualizarQualidade(): Item[] {
    this.itens = this.itens.map((item: Item) =>
      item.nome !== 'Dente do Tarrasque'
        ? this.atualizarItem(item)
        : item
    );
    return this.itens;
  }

  private atualizarItem(item: Item): Item {
    if (item.nome.includes('Queijo Brie Envelhecido')) {
      return new QueijoBrieEnvelhecido(item).atualizarItem();
    }
    if (item.nome.includes('Ingressos para o concerto do Turisas')) {
      return new IngressoParaConcertoTurisas(item).atualizarItem();
    }
    return new ItemAtualizavel(item).atualizarItem();
  }
}
