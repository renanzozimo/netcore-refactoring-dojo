import { ItemAtualizavel } from './ItemAtualizavel';

export class IngressoParaConcertoTurisas extends ItemAtualizavel {

  atualizarQualidade(): void {
    if (this.prazoParaVendaExpirado()) {
      this.qualidade = 0;
    } else {
      super.atualizarQualidade();
    }
  }

  calcularVariaçãoQualidade(): number {
    if (this.prazoParaVenda <= 5) {
      return 3;
    }
    if (this.prazoParaVenda <= 10) {
      return 2;
    }
    return 1;
  }
}
