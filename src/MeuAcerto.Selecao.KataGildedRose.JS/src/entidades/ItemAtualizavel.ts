import { MathHelpers } from '../helpers/MathHelpers';
import { Item } from './Item';

export class ItemAtualizavel extends Item {
  qualidadeMínima: number = 0;
  qualidadeMáxima: number = 50;

  constructor(item: Item) {
    super(item.nome, item.prazoParaVenda, item.qualidade);
  }

  atualizarItem(): Item {
    this.atualizarQualidade();
    this.atualizarPrazoParaVenda();
    return new Item(this.nome, this.prazoParaVenda, this.qualidade);
  }

  atualizarPrazoParaVenda(): void {
    --this.prazoParaVenda;
  }

  atualizarQualidade(): void {
    this.qualidade =
      this.qualidade +
      this.calcularVariaçãoQualidade() * (this.foiConjurado() ? 2 : 1);
    this.limitarQualidade();
  }

  limitarQualidade(): void {
    this.qualidade = MathHelpers
      .limitarValor(this.qualidadeMínima, this.qualidade, this.qualidadeMáxima);
  }

  calcularVariaçãoQualidade(): number {
    return this.prazoParaVendaExpirado() ? -2 : -1;
  }

  foiConjurado(): boolean {
    return this.nome.toUpperCase().includes('CONJURADO');
  }

  prazoParaVendaExpirado(): boolean {
    return this.prazoParaVenda <= 0;
  }
}
