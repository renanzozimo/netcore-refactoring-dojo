import { ItemAtualizavel } from './ItemAtualizavel';

export class QueijoBrieEnvelhecido extends ItemAtualizavel {
  calcularVariaçãoQualidade(): number {
    return this.prazoParaVendaExpirado() ? 2 : 1;
  }
}
