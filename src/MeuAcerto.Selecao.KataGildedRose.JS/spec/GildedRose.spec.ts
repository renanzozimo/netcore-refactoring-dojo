import { GildedRose } from '../src/entidades/GildedRose';
import { Item } from '../src/entidades/Item';

describe('GildedRose', function() {

  const listaBase = [
    new Item('Dente do Tarrasque', -1, 80),
    new Item('Dente do Tarrasque', 0, 80),
    new Item('Ingressos para o concerto do Turisas', 1, 45),
    new Item('Queijo Brie Envelhecido conjurado', 0, 0),
    new Item('Queijo Brie Envelhecido', 2, 0),
    new Item('Queijo Brie Envelhecido', -1, 0),
    new Item('Queijo', 0, 2),
  ];

  describe('quando atualizar uma lista usando atualizarItens', function() {
    it('deve atualizar todos os itens da lista exceto Dente do Tarrasque ', function() {
      expect(
        new GildedRose(listaBase).atualizarQualidade()
      ).toEqual(
        [
          jasmine.objectContaining(
            {
              nome: 'Dente do Tarrasque',
              prazoParaVenda: -1,
              qualidade: 80,
            }
          ),
          jasmine.objectContaining(
            {
              nome: 'Dente do Tarrasque',
              prazoParaVenda: 0,
              qualidade: 80,
            },
          ),
          jasmine.objectContaining(
            {
              nome: 'Ingressos para o concerto do Turisas',
              prazoParaVenda: 0,
              qualidade: 48,
            },
          ),
          jasmine.objectContaining(
            {
              nome: 'Queijo Brie Envelhecido conjurado',
              prazoParaVenda: -1,
              qualidade: 4,
            },
          ),
          jasmine.objectContaining(
            {
              nome: 'Queijo Brie Envelhecido',
              prazoParaVenda: 1,
              qualidade: 1,
            },
          ),
          jasmine.objectContaining(
            {
              nome: 'Queijo Brie Envelhecido',
              prazoParaVenda: -2,
              qualidade: 2,
            },
          ),
          jasmine.objectContaining(
            {
              nome: 'Queijo',
              prazoParaVenda: -1,
              qualidade: 0,
            },
          ),
        ]
      );
    });
  });

});
