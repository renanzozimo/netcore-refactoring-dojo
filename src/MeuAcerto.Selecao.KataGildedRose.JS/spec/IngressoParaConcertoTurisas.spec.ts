import { IngressoParaConcertoTurisas } from '../src/entidades/IngressoParaConcertoTurisas';

describe('IngressoParaConcertoTurisas', function() {
  const itemBase = {
    nome: 'Ingressos para o concerto do Turisas',
    prazoParaVenda: 0,
    qualidade: 0,
  };

  describe('ao calcularVariaçãoQualidade', function() {
    it('deve retornar aumento de 1 de qualidade havendo pelo menos 11 de prazoParaVenda', function() {
      expect(
        new IngressoParaConcertoTurisas({ ...itemBase, prazoParaVenda: 11 })
          .calcularVariaçãoQualidade()
      ).toEqual(1);
    });

    it('deve retornar aumento de 2 de qualidade havendo até 10 de prazoParaVenda', function() {
      expect(
        new IngressoParaConcertoTurisas({ ...itemBase, prazoParaVenda: 10 })
          .calcularVariaçãoQualidade()
      ).toEqual(2);
    });

    it('deve retornar aumento de 2 de qualidade havendo pelo menos 6 de prazoParaVenda', function() {
      expect(
        new IngressoParaConcertoTurisas({ ...itemBase, prazoParaVenda: 6 })
          .calcularVariaçãoQualidade()
      ).toEqual(2);
    });

    it('deve retornar aumento de 3 de qualidade havendo até 5 de prazoParaVenda', function() {
      expect(
        new IngressoParaConcertoTurisas({ ...itemBase, prazoParaVenda: 5 })
          .calcularVariaçãoQualidade()
      ).toEqual(3);
    });

    it('deve retornar aumento de 3 de qualidade havendoelo menos 1 de prazoParaVenda', function() {
      expect(
        new IngressoParaConcertoTurisas({ ...itemBase, prazoParaVenda: 5 })
          .calcularVariaçãoQualidade()
      ).toEqual(3);
    });
  });

});
