import { QueijoBrieEnvelhecido } from '../src/entidades/QueijoBrieEnvelhecido';

describe('QueijoBrieEnvelhecido', function() {
  const itemBase = {
    nome: 'Queijo Brie Envelhecido',
    prazoParaVenda: 0,
    qualidade: 0,
  };

  describe('ao calcularVariaçãoQualidade', function() {
    it('deve retornar aumento de 1 de qualidade quando ainda houver prazoParaVenda', function() {
      expect(
        new QueijoBrieEnvelhecido({ ...itemBase, prazoParaVenda: 1 })
          .calcularVariaçãoQualidade()
      ).toEqual(1);
    });

    it('deve retornar aumento de 2 de qualidade quando não houver mais prazoParaVenda', function() {
      expect(
        new QueijoBrieEnvelhecido(itemBase)
          .calcularVariaçãoQualidade()
      ).toEqual(2);
    });
  });

});
