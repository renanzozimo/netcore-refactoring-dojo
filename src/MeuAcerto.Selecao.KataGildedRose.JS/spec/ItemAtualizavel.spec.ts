import { IngressoParaConcertoTurisas } from '../src/entidades/IngressoParaConcertoTurisas';
import { ItemAtualizavel } from '../src/entidades/ItemAtualizavel';
import { QueijoBrieEnvelhecido } from '../src/entidades/QueijoBrieEnvelhecido';

describe('ItemAtualizavel', function() {
  const itemBase = {
    nome: 'Item genérico',
    prazoParaVenda: 0,
    qualidade: 0,
  };
  const itemAtualizavel = new ItemAtualizavel(itemBase);

  const limitarQualidade = (itemData: { [key: string]: unknown }) => {
    const novoItem: ItemAtualizavel =
      new ItemAtualizavel({ ...itemBase, ...itemData });
    novoItem.limitarQualidade();
    return novoItem;
  };

  beforeEach(function() {
    itemBase.nome = 'Item genérico';
    itemBase.prazoParaVenda = 0;
    itemBase.qualidade = 0;
  });

  describe('quando verificado prazoParaVendaExpirado', function() {
    it('deve retornar verdadeiro, ou seja, esgotado, prazoParaVenda 0', function() {
      expect(
        new ItemAtualizavel({
          ...itemBase,
          prazoParaVenda: 0,
        }).prazoParaVendaExpirado()
      ).toEqual(true);
    });

    it('deve retornar verdadeiro, ou seja, esgotado, prazoParaVenda negativo', function() {
      expect(
        new ItemAtualizavel({
          ...itemBase,
          prazoParaVenda: -1,
        }).prazoParaVendaExpirado()
      ).toEqual(true);
    });

    it('deve retornar falso, ou seja, ainda dentro do prazo prazoParaVenda 1', function() {
      expect(
        new ItemAtualizavel({
          ...itemBase,
          prazoParaVenda: 1,
        }).prazoParaVendaExpirado()
      ).toEqual(false);
    });
  });

  describe('quando o item foiConjurado', function() {
    it('deve retornar verdadeiro para item de nome "Item conjurado"' , function() {
      expect(
        new ItemAtualizavel({
          ...itemBase,
          nome: 'Item conjurado',
        }).foiConjurado()
      ).toEqual(true);
    });

    it('deve retornar verdadeiro para item de nome "Conjurado item"' , function() {
      expect(
        new ItemAtualizavel({
          ...itemBase,
          nome: 'Conjurado item',
        }).foiConjurado()
      ).toEqual(true);
    });

    it('deve retornar falso para item de nome "Item conju"' , function() {
      expect(
        new ItemAtualizavel({
          ...itemBase,
          nome: 'Item conju',
        }).foiConjurado()
      ).toEqual(false);
    });
  });

  describe('quando calcularVariaçãoQualidade de item comum', function() {
    it('deve retornar -1 quando ainda houver prazoParaVenda', function() {
      expect(
        new ItemAtualizavel({ ...itemBase, prazoParaVenda: 1 })
          .calcularVariaçãoQualidade()
      ).toEqual(-1);
    });

    it('deve retornar -2 quando não houver mais prazoParaVenda', function() {
      expect(
        new ItemAtualizavel({ ...itemBase, prazoParaVenda: 0 })
          .calcularVariaçãoQualidade()
      ).toEqual(-2);
    });
  });

  describe('quando usado limitarQualidade', function() {
    it('a qualidade deve ser limitada ao máximo', function() {
      expect(
        limitarQualidade({ qualidade: itemAtualizavel.qualidadeMáxima + 10 })
      ).toEqual(
        jasmine.objectContaining<ItemAtualizavel>({
          qualidade: itemAtualizavel.qualidadeMáxima,
        })
      );
    });

    it('a qualidade deve ser limitada ao mínimo', function() {
      expect(
        limitarQualidade({ qualidade: itemAtualizavel.qualidadeMínima - 10 })
      ).toEqual(
        jasmine.objectContaining<ItemAtualizavel>({
          qualidade: itemAtualizavel.qualidadeMínima,
        })
      );
    });

    it('a qualidade não deve ser alterada caso não exceda máximo e mínimo', function() {
      const qualidade: number =
        (itemAtualizavel.qualidadeMáxima - itemAtualizavel.qualidadeMínima) / 2;
      const novaQualidade: number =
        (itemAtualizavel.qualidadeMáxima - itemAtualizavel.qualidadeMínima) / 2;
      expect(
        limitarQualidade({ qualidade })
      ).toEqual(
        jasmine.objectContaining<ItemAtualizavel>({ qualidade: novaQualidade })
      );
    });
  });

  describe('quando atualizaItem', function() {
    it('deve aumentar em 1 a qualidade do QueijoBrieEnvelhecido com prazoDeVenda 3', function() {
      expect(
        new QueijoBrieEnvelhecido({
          nome: 'Queijo Brie Envelhecido',
          prazoParaVenda: 3,
          qualidade: 0,
        }).atualizarItem()
      ).toEqual(
        jasmine.objectContaining<ItemAtualizavel>({
          prazoParaVenda: 2,
          qualidade: 1,
        })
      );
    });

    it('deve aumentar em 2 a qualidade do QueijoBrieEnvelhecido com prazoDeVenda 0', function() {
      expect(
        new QueijoBrieEnvelhecido({
          nome: 'Queijo Brie Envelhecido',
          prazoParaVenda: 0,
          qualidade: 0,
        }).atualizarItem()
      ).toEqual(
        jasmine.objectContaining<ItemAtualizavel>({
          prazoParaVenda: -1,
          qualidade: 2,
        })
      );
    });

    it('deve aumentar em 2 a qualidade do QueijoBrieEnvelhecido com prazoDeVenda negativo', function() {
      expect(
        new QueijoBrieEnvelhecido({
          nome: 'Queijo Brie Envelhecido',
          prazoParaVenda: -2,
          qualidade: 0,
        }).atualizarItem()
      ).toEqual(
        jasmine.objectContaining<ItemAtualizavel>({
          prazoParaVenda: -3,
          qualidade: 2,
        })
      );
    });

    it('não deve exceder o limite máximo mesmo aumentando a qualidade do QueijoBrieEnvelhecido', function() {
      expect(
        new QueijoBrieEnvelhecido({
          nome: 'Queijo Brie Envelhecido',
          prazoParaVenda: -5,
          qualidade: itemAtualizavel.qualidadeMáxima - 1,
        }).atualizarItem()
      ).toEqual(
        jasmine.objectContaining<ItemAtualizavel>({
          prazoParaVenda: -6,
          qualidade: itemAtualizavel.qualidadeMáxima,
        })
      );
    });

    it('deve aumentar em 1 a qualidade do IngressoParaConcertoTurisas com prazoDeVenda 16', function() {
      expect(
        new QueijoBrieEnvelhecido({
          nome: 'Ingressos para o concerto do Turisas',
          prazoParaVenda: 16,
          qualidade: 20,
        }).atualizarItem()
      ).toEqual(
        jasmine.objectContaining<ItemAtualizavel>({
          prazoParaVenda: 15,
          qualidade: 21,
        })
      );
    });

    it('deve aumentar em 1 a qualidade do IngressoParaConcertoTurisas com prazoDeVenda 11', function() {
      expect(
        new QueijoBrieEnvelhecido({
          nome: 'Ingressos para o concerto do Turisas',
          prazoParaVenda: 11,
          qualidade: 20,
        }).atualizarItem()
      ).toEqual(
        jasmine.objectContaining<ItemAtualizavel>({
          prazoParaVenda: 10,
          qualidade: 21,
        })
      );
    });

    it('deve aumentar em 2 a qualidade do IngressoParaConcertoTurisas com prazoDeVenda 10', function() {
      expect(
        new IngressoParaConcertoTurisas({
          nome: 'Ingressos para o concerto do Turisas',
          prazoParaVenda: 10,
          qualidade: 20,
        }).atualizarItem()
      ).toEqual(
        jasmine.objectContaining<ItemAtualizavel>({
          prazoParaVenda: 9,
          qualidade: 22,
        })
      );
    });

    it('deve aumentar em 2 a qualidade do IngressoParaConcertoTurisas com prazoDeVenda 6', function() {
      expect(
        new IngressoParaConcertoTurisas({
          nome: 'Ingressos para o concerto do Turisas',
          prazoParaVenda: 6,
          qualidade: 20,
        }).atualizarItem()
      ).toEqual(
        jasmine.objectContaining<ItemAtualizavel>({
          prazoParaVenda: 5,
          qualidade: 22,
        })
      );
    });

    it('deve aumentar em 3 a qualidade do IngressoParaConcertoTurisas com prazoDeVenda 5', function() {
      expect(
        new IngressoParaConcertoTurisas({
          nome: 'Ingressos para o concerto do Turisas',
          prazoParaVenda: 5,
          qualidade: 20,
        }).atualizarItem()
      ).toEqual(
        jasmine.objectContaining<ItemAtualizavel>({
          prazoParaVenda: 4,
          qualidade: 23,
        })
      );
    });

    it('deve aumentar em 3 a qualidade do IngressoParaConcertoTurisas com prazoDeVenda 1', function() {
      expect(
        new IngressoParaConcertoTurisas({
          nome: 'Ingressos para o concerto do Turisas',
          prazoParaVenda: 1,
          qualidade: 20,
        }).atualizarItem()
      ).toEqual(
        jasmine.objectContaining<ItemAtualizavel>({
          prazoParaVenda: 0,
          qualidade: 23,
        })
      );
    });

    it('deve zerar a qualidade do IngressoParaConcertoTurisas com prazoDeVenda 0', function() {
      expect(
        new IngressoParaConcertoTurisas({
          nome: 'Ingressos para o concerto do Turisas',
          prazoParaVenda: 0,
          qualidade: 20,
        }).atualizarItem()
      ).toEqual(
        jasmine.objectContaining<ItemAtualizavel>({
          prazoParaVenda: -1,
          qualidade: 0,
        })
      );
    });

    it('deve zerar a qualidade do IngressoParaConcertoTurisas sem prazoDeVenda', function() {
      expect(
        new IngressoParaConcertoTurisas({
          nome: 'Ingressos para o concerto do Turisas',
          prazoParaVenda: -5,
          qualidade: 0,
        }).atualizarItem()
      ).toEqual(
        jasmine.objectContaining<ItemAtualizavel>({
          prazoParaVenda: -6,
          qualidade: 0,
        })
      );
    });

    it('deve diminuir em 1 a qualidade de um item ainda com prazoParaVenda', function() {
      expect(
        new ItemAtualizavel({
          nome: 'Ingressos para o concerto do Turisas',
          prazoParaVenda: 10,
          qualidade: 20,
        }).atualizarItem()
      ).toEqual(
        jasmine.objectContaining<ItemAtualizavel>({
          prazoParaVenda: 9,
          qualidade: 19,
        })
      );
    });

    it('deve diminuir em 2 a qualidade de um item com prazoParaVenda 0', function() {
      expect(
        new ItemAtualizavel({
          nome: 'Ingressos para o concerto do Turisas',
          prazoParaVenda: 0,
          qualidade: 20,
        }).atualizarItem()
      ).toEqual(
        jasmine.objectContaining<ItemAtualizavel>({
          prazoParaVenda: -1,
          qualidade: 18,
        })
      );
    });

    it('deve diminuir em 2 a qualidade de um item com prazoParaVenda negativo', function() {
      expect(
        new ItemAtualizavel({
          nome: 'Ingressos para o concerto do Turisas',
          prazoParaVenda: -10,
          qualidade: 20,
        }).atualizarItem()
      ).toEqual(
        jasmine.objectContaining<ItemAtualizavel>({
          prazoParaVenda: -11,
          qualidade: 18,
        })
      );
    });

  });
});
