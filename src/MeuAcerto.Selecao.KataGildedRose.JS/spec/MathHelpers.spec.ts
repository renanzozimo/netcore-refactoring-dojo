import { MathHelpers } from '../src/helpers/MathHelpers';

describe('MathHelpers', function() {
  let limitador: (min: number, value: number, max: number) => number;
  beforeEach(function() {
    limitador = MathHelpers.limitarValor;
  });

  describe('quando o limitador é usado para obter um valor obedecendo limites mínimo e máximo', function() {
    it('deve ser capaz de limitar 51 tendo 30 como mínimo e 50 como máximo', function() {
      expect(limitador(30,51,50)).toEqual(50);
    });
    it('deve ser capaz de limitar 29 tendo 30 como mínimo e 50 como máximo', function() {
      expect(limitador(30,29,50)).toEqual(30);
    });
    it('deve ser capaz de limitar 10 tendo 5 como mínimo e 5 como máximo', function() {
      expect(limitador(5,10,5)).toEqual(5);
    });
    it('deve ser capaz de limitar 51 tendo 30 como mínimo e 50 como máximo', function() {
      expect(limitador(50,51,30)).toEqual(50);
    });
  });
});
