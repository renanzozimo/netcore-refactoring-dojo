module.exports = {
  'env': {
    'browser': true,
    'es6': true,
    'node': true
  },
  'parser': '@typescript-eslint/parser',
  'parserOptions': {
    'tsconfigRootDir': __dirname,
    'project': 'tsconfig.json',
    'sourceType': 'module',
  },
  'extends': [
    'plugin:@typescript-eslint/recommended',
    'plugin:@typescript-eslint/recommended-requiring-type-checking',
    'prettier',
  ],
  'plugins': [
    '@typescript-eslint',
  ],
  'ignorePatterns': ['**/*.js'],
  'rules': {
    'arrow-body-style': ['error', 'as-needed'],
    'arrow-parens': [
      'error',
      'as-needed'
    ],
    'object-curly-spacing': ['error', 'always'],
    'brace-style': [
      'error',
      '1tbs'
    ],
    'camelcase': 'error',
    'comma-dangle': [
      'error',
      'always-multiline'
    ],
    'complexity': 'off',
    'constructor-super': 'error',
    'curly': 'error',
    'eol-last': 'error',
    'eqeqeq': [
      'error',
      'smart'
    ],
    'guard-for-in': 'error',
    'id-blacklist': 'off',
    'id-match': 'error',
    'import/order': 'off',
    'keyword-spacing': ['error', { 'before': true, 'after': true }],
    'max-classes-per-file': 'off',
    'max-len': [
      'error',
      {
        'code': 80,
        'ignoreStrings': true,
        'ignoreTemplateLiterals': true,
        'ignoreComments': true,
        'ignorePattern': '^import\\s.+\\sfrom\\s.+;$',
        'ignoreUrls': true
      }
    ],
    'new-parens': 'error',
    'no-bitwise': 'error',
    'no-caller': 'error',
    'no-console': ["error", { allow: ["warn", "error"] }],
    'no-debugger': 'error',
    'no-duplicate-imports': 'error',
    'no-empty': 'off',
    'no-eval': 'error',
    'no-fallthrough': 'error',
    'no-inline-comments': 'error',
    'no-invalid-this': 'off',
    'no-multiple-empty-lines': ['error', { 'max': 1 }],
    'no-multi-spaces': 'error',
    'no-new-wrappers': 'error',
    'no-restricted-imports': [
      'error',
      'rxjs/Rx'
    ],
    'no-shadow': [
      'error',
      {
        'hoist': 'all'
      }
    ],
    'no-throw-literal': 'error',
    'no-trailing-spaces': 'error',
    'no-undef-init': 'error',
    'no-underscore-dangle': 'off',
    'no-unsafe-finally': 'error',
    'no-unused-labels': 'error',
    'no-var': 'error',
    'no-warning-comments': 'error',
    'object-shorthand': 'error',
    'one-var': [
      'error',
      'never'
    ],
    'quote-props': [
      'error',
      'as-needed'
    ],
    'radix': 'error',
    'space-before-function-paren': [
      'error',
      {
        'anonymous': 'never',
        'asyncArrow': 'always',
        'named': 'never'
      }
    ],
    'space-before-blocks': 'error',
    'spaced-comment': [
      'error',
      'always',
      {
        'markers': [
          '/'
        ]
      }
    ],
    'use-isnan': 'error',
    'valid-typeof': 'off',
    // TYPESCRIPT
    '@typescript-eslint/array-type': 'off',
    '@typescript-eslint/ban-types': [
      'error',
      {
        'types': {
          'Object': {
            'message': 'Avoid using the `Object` type. Did you mean `object`?'
          },
          'Function': {
            'message': 'Avoid using the `Function` type. Prefer a specific function type, like `() => void`.'
          },
          'Boolean': {
            'message': 'Avoid using the `Boolean` type. Did you mean `boolean`?'
          },
          'Number': {
            'message': 'Avoid using the `Number` type. Did you mean `number`?'
          },
          'String': {
            'message': 'Avoid using the `String` type. Did you mean `string`?'
          },
          'Symbol': {
            'message': 'Avoid using the `Symbol` type. Did you mean `symbol`?'
          }
        }
      }
    ],
    '@typescript-eslint/consistent-type-definitions': 'error',
    '@typescript-eslint/explicit-member-accessibility': [
      'off',
      {
        'accessibility': 'explicit'
      }
    ],
    '@typescript-eslint/indent': [
      'error',
      2,
      {
        'FunctionDeclaration': {
          'parameters': 'first'
        },
        'FunctionExpression': {
          'parameters': 'first'
        },
        'SwitchCase': 1
      }
    ],
    '@typescript-eslint/member-delimiter-style': [
      'error',
      {
        'multiline': {
          'delimiter': 'semi',
          'requireLast': true
        },
        'singleline': {
          'delimiter': 'semi',
          'requireLast': false
        }
      }
    ],
    '@typescript-eslint/no-inferrable-types': 'off',
    '@typescript-eslint/member-ordering': 'error',
    '@typescript-eslint/no-empty-function': 'off',
    '@typescript-eslint/no-explicit-any': 'error',
    '@typescript-eslint/no-non-null-assertion': 'error',
    '@typescript-eslint/no-parameter-properties': 'off',
    '@typescript-eslint/unbound-method': [
      'error',
      {
        'ignoreStatic': true
      }
    ],
    '@typescript-eslint/no-unnecessary-type-assertion' : 'off',
    '@typescript-eslint/no-unused-expressions': 'error',
    '@typescript-eslint/no-unsafe-return': 'error',
    '@typescript-eslint/no-unsafe-assignment': 'off',
    '@typescript-eslint/no-unused-vars': ['error', { 'argsIgnorePattern': '_' }],
    '@typescript-eslint/no-var-requires': 'off',
    '@typescript-eslint/prefer-for-of': 'error',
    '@typescript-eslint/prefer-function-type': 'error',
    '@typescript-eslint/quotes': [
      'error',
      'single'
    ],
    '@typescript-eslint/semi': [
      'error',
      'always'
    ],
    '@typescript-eslint/triple-slash-reference': [
      'error',
      {
        'path': 'always',
        'types': 'prefer-import',
        'lib': 'always'
      }
    ],
    '@typescript-eslint/unified-signatures': 'error',
    '@typescript-eslint/typedef': [
      'error',
      {
        'parameter': true,
        'arrowParameter': true,
        'memberVariableDeclaration': true,
        'propertyDeclaration': true,
        'variableDeclarationIgnoreFunction': true
      }
    ],
  },
};
